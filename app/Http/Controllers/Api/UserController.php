<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegister;
use App\Http\Requests\UserUpdate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{
	/**
	 * Register user
	 *
	 * @param UserRegister $request
	 *
	 * @return User
	 */
	public function register(UserRegister $request)
	{
		$data = $request->all();

		return User::create(array_merge($data, [
			'password' => Hash::make($data['password']),
		]));
	}

	/**
	 * Logout action
	 */
	public function logout()
	{
		if (Auth::id()) {
			Auth::user()->authAcessToken()->delete();
		}

		return ['result' => true];
	}

	/**
	 * Self
	 *
	 * @param Request $request
	 *
	 * @return User
	 */
	public function self(Request $request)
	{
		return $request->user();
	}

	/**
	 * Updating user
	 *
	 * @param UserUpdate $request
	 *
	 * @return User
	 */
	public function update(UserUpdate $request)
	{
		$data = $request->all();

		$user = Auth::user();
		$user->fill(array_filter($data));
		$user->save();

		return $user;
	}
}
