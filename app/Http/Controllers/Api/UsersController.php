<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
	/**
	 * Getting all user
	 *
	 * @return User[]
	 */
	public function all()
	{
		$users = User::get();

		return $users->map(function ($user) {
			return collect($user->toArray())
				->only(['id', 'name', 'last_name', 'registration_date'])
				->all();
		});
	}

	/**
	 * Getting user by id
	 *
	 * @param int $id
	 *
	 * @return User
	 */
	public function one($id)
	{
		return User::findOrFail($id);
	}

	/**
	 * Like other user
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return array
	 */
	public function like(Request $request, $id)
	{
		if ($request->user()->id == $id) {
			throw new \InvalidArgumentException('You can\'t like yourself');
		}

		$model = User::findOrFail($id);

		return ['result' => $model->addLike()];
	}
}
