<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    const CREATED_AT = 'registration_date';
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'registration_date' => 'datetime:d M Y',
    ];

	/**
	 * @var array
	 */
    protected $appends = [
    	'likes'
	];

	/**
	 * Aauth Acess Token
	 *
	 * @return HasMany
	 */
	public function authAcessToken() {
		return $this->hasMany('\App\OauthAccessToken');
	}

	/**
	 * Likes
	 *
	 * @return HasMany
	 */
	public function likes()
	{
		return $this->hasMany('\App\Like', 'user_for_id');
	}

	/**
	 * Add Like
	 *
	 * @return bool
	 */
	public function addLike()
	{
		$userId = Auth::id();

		if ($this->id == $userId) {
			return false;
		}

		if ($this->likes()->where('user_id', '=', $userId)->exists()) {
			return false;
		}

		$like = new Like(['user_id' => $userId]);

		return (bool) $this->likes()->save($like);
	}

	/**
	 * Get Likes Attribute
	 *
	 * @return int
	 */
	public function getLikesAttribute()
	{
		return $this->likes()->count();
	}
}
