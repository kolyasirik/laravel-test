<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
	Route::middleware('guest')->post('user', 'UserController@register');

	Route::middleware('auth:api')->group(function () {
		Route::prefix('user')->group(function () {
			Route::get('', 'UserController@self');
			Route::put('', 'UserController@update');
			Route::post('logout', 'UserController@logout');
		});

		Route::prefix('users')->group(function () {
			Route::get('', 'UsersController@all');
			Route::get('{id}', 'UsersController@one');
			Route::get('{id}/like', 'UsersController@like');
		});
	});
});
